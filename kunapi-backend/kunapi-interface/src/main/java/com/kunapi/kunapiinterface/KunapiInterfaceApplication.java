package com.kunapi.kunapiinterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KunapiInterfaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(KunapiInterfaceApplication.class, args);
    }

}
