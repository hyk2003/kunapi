-- 接口信息
DROP TABLE IF EXISTS `interface_info`;
CREATE TABLE `interface_info`
(
    `id`                   bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`                 varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
    `description`          varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL     DEFAULT NULL COMMENT '描述',
    `url`                  varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口地址',
    `host`                 varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL     DEFAULT NULL COMMENT '主机名',
    `requestParams`        text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         NULL COMMENT '请求参数',
    `requestParamsRemark`  text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         NULL COMMENT '请求参数说明',
    `responseParamsRemark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         NULL COMMENT '响应参数说明',
    `requestHeader`        text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         NULL COMMENT '请求头',
    `responseHeader`       text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci         NULL COMMENT '响应头',
    `status`               int                                                           NOT NULL DEFAULT 0 COMMENT '接口状态（0-关闭，1-开启）',
    `method`               varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求类型',
    `userId`               bigint                                                        NOT NULL COMMENT '创建人',
    `createTime`           datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updateTime`           datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `isDelete`             tinyint                                                       NOT NULL DEFAULT 0 COMMENT '是否删除(0-未删, 1-已删)',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 30
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '接口信息'
  ROW_FORMAT = Dynamic;

insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('雷天磊', '杜果', 'www.dean-lakin.net', '黄文轩', '方苑博', 0, '傅靖琪', 69);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('彭鹏飞', '沈文博', 'www.ray-jenkins.co', '林博涛', '曾明杰', 0, '邵浩', 884);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('金越彬', '魏烨霖', 'www.margarett-corwin.net', '赖智辉', '魏峻熙', 0, '魏绍辉', 1668417);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('龙懿轩', '高鑫鹏', 'www.felton-metz.com', '江子默', '江哲瀚', 0, '严明', 39592);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('薛天磊', '龙凯瑞', 'www.carmina-daugherty.biz', '朱钰轩', '李炎彬', 0, '赵烨华', 9484662558);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('黄鹤轩', '郭锦程', 'www.isaiah-gusikowski.io', '程昊然', '贺峻熙', 0, '钟峻熙', 5605960608);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('崔涛', '张立诚', 'www.art-yundt.net', '朱昊强', '杜文轩', 0, '崔立轩', 15);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('冯嘉熙', '邹明哲', 'www.matilde-howell.com', '罗鹤轩', '尹子默', 0, '雷涛', 119782);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('郝风华', '彭越彬', 'www.randell-donnelly.com', '胡博文', '苏浩宇', 0, '万果', 424);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('萧思源', '陈俊驰', 'www.homer-spencer.co', '曹聪健', '郝彬', 0, '崔天磊', 2881);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('张弘文', '彭伟泽', 'www.bret-rice.info', '孙哲瀚', '熊语堂', 0, '段智宸', 59530);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('沈志泽', '杨绍齐', 'www.angie-vonrueden.co', '唐博涛', '范锦程', 0, '侯立辉', 8279354);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('邓擎宇', '孟明', 'www.adolph-tromp.org', '龚瑞霖', '杜绍齐', 0, '马烨磊', 580009);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('傅涛', '贾智辉', 'www.wilda-konopelski.co', '宋浩轩', '李涛', 0, '孔立轩', 9406719570);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('廖笑愚', '曹修杰', 'www.teisha-johns.com', '周致远', '雷彬', 0, '石涛', 148347640);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('姚昊天', '邓智辉', 'www.sharlene-hagenes.co', '何君浩', '黄昊天', 0, '陶浩', 550);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('廖烨华', '余绍辉', 'www.elvera-morar.org', '史健雄', '龚弘文', 0, '莫智渊', 70);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('戴智辉', '胡鑫鹏', 'www.cherrie-deckow.net', '唐晓啸', '江致远', 0, '龙伟宸', 431685639);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('于展鹏', '袁钰轩', 'www.clora-heidenreich.name', '金驰', '姜明辉', 0, '朱嘉懿', 57829);
insert into kunapi.`interface_info` (`name`, `description`, `url`, `requestHeader`, `responseHeader`, `status`,
                                     `method`, `userId`)
values ('阎子默', '龙正豪', 'www.gregoria-brekke.io', '武涛', '冯思聪', 0, '郭志泽', 1);

-- 用户调用接口关系表
create table if not exists kunapi.`user_interface_info`
(
    `id`              bigint                             not null auto_increment comment '主键' primary key,
    `userId`          bigint                             not null comment '调用人 Id',
    `interfaceInfoId` bigint                             not null comment '调用接口 Id',
    `totalNum`        int      default 0                 not null comment '总调用次数',
    `leftNum`         int      default 0                 not null comment '剩余调用次数',
    `status`          int      default 0                 not null comment '0-正常， 1-禁用',
    `createTime`      datetime default CURRENT_TIMESTAMP not null comment '创建时间',
    `updateTime`      datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '更新时间',
    `isDelete`        tinyint  default 0                 not null comment '是否删除(0-未删, 1-已删)'
) comment '用户调用接口关系表';