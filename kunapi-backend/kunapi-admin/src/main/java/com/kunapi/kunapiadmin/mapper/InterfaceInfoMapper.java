package com.kunapi.kunapiadmin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kunapi.kunapicommon.model.entity.InterfaceInfo;

/**
* @author 65191
 *
*/
public interface InterfaceInfoMapper extends BaseMapper<InterfaceInfo> {

}




