package com.kunapi.kunapiadmin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kunapi.kunapicommon.model.entity.InterfaceInfo;
import com.kunapi.kunapicommon.model.entity.UserInterfaceInfo;
import org.apache.ibatis.annotations.Select;

import java.util.List;

import static net.sf.jsqlparser.parser.feature.Feature.select;

/**
* @author 65191
* @description 针对表【user_interface_info(用户调用接口关系表)】的数据库操作Mapper
* @createDate 2023-09-19 11:12:00
* @Entity generator.domain.UserInterfaceInfo
*/
public interface UserInterfaceInfoMapper extends BaseMapper<UserInterfaceInfo> {

    List<UserInterfaceInfo> listTopInvokeInterfaceInfo(int limit);
}




