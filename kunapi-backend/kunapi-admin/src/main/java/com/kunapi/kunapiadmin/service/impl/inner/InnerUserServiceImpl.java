package com.kunapi.kunapiadmin.service.impl.inner;

import com.kunapi.kunapiadmin.exception.BusinessException;
import com.kunapi.kunapiadmin.service.UserService;
import com.kunapi.kunapicommon.common.ErrorCode;
import com.kunapi.kunapicommon.model.entity.User;
import com.kunapi.kunapicommon.service.InnerUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;

/**
 * 用户服务实现
 *
 * @author <a href="https://github.com/liyupi">程序员鱼皮</a>
 * @from <a href="https://yupi.icu">编程导航知识星球</a>
 */
@DubboService
public class InnerUserServiceImpl implements InnerUserService {


    @Resource
    UserService userService;

    @Override
    public User getInvokeUser(String accessKey) {
        if(StringUtils.isAnyBlank(accessKey)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        return userService.query()
                .eq("accessKey", accessKey)
                .one();
    }
}
