package com.kunapi.kunapiclientsdk.client;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.kunapi.kunapiclientsdk.model.User;
import org.springframework.core.env.Environment;


import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.kunapi.kunapiclientsdk.utils.SignUtils.gensign;
/**
 * @author Catoy
 */
public class KunApiClient {
    public String accessKey;

    public String secretKey;

    public String gatewayHost = "http://localhost:8090";


    public KunApiClient(String accessKey, String secretKey) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }

    public void setGatewayHost(String gatewayHost) {
        this.gatewayHost = gatewayHost;
    }

    private Map<String, String> getHeaderMap(String body, String method) throws UnsupportedEncodingException{
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("accessKey", accessKey);
        // hashMap.put("secretKey", secretKey);
        hashMap.put("nonce", RandomUtil.randomNumbers(4));
        body = URLUtil.encode(body, CharsetUtil.CHARSET_UTF_8);
        hashMap.put("body", body);
        hashMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
        hashMap.put("sign", gensign(body, secretKey));
        hashMap.put("method", method);
        return hashMap;
    }


    public String invokeInterface(String params, String url, String method) throws UnsupportedEncodingException {
        HttpResponse httpResponse = HttpRequest.post(gatewayHost + url)
                .header("Accept-Charset", CharsetUtil.UTF_8)
                .addHeaders(getHeaderMap(params, method))
                .body(params)
                .execute();
        return JSONUtil.formatJsonStr(httpResponse.body());
    }
}
