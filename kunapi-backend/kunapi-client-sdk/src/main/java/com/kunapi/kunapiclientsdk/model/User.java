package com.kunapi.kunapiclientsdk.model;

import lombok.Data;

@Data
public class User {
    private String username;
}
