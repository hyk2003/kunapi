package com.kunapi.kunapiclientsdk;

import com.kunapi.kunapiclientsdk.client.KunApiClient;
import lombok.Data;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Catoy
 */

@Configuration
@ConfigurationProperties("kun.client")
@Data
@ComponentScan
public class KunApiClientConfig {
    private String accessKey;

    private String secretKey;

    @Bean
    public KunApiClient kunApiClient() {
        return new KunApiClient(accessKey, secretKey);
    }
}
